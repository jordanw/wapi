import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  { path: 'resevatorio', loadChildren: './resevatorio/resevatorio.module#ResevatorioPageModule' },
  { path: 'dados', loadChildren: './dados/dados.module#DadosPageModule' },
  { path: 'especies', loadChildren: './especies/especies.module#EspeciesPageModule' },
  { path: 'sensores', loadChildren: './sensores/sensores.module#SensoresPageModule' },
  { path: 'graficos', loadChildren: './graficos/graficos.module#GraficosPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
