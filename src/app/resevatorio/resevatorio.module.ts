import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ResevatorioPage } from './resevatorio.page';

const routes: Routes = [
  {
    path: '',
    component: ResevatorioPage,children:
    [
      {
        path: 'dados',
        children:
          [
            {
              path: '',
              loadChildren: '../dados/dados.module#DadosPageModule'
            }
          ]
      },
      {
        path: 'especies',
        children:
          [
            {
              path: '',
              loadChildren: '../especies/especies.module#EspeciesPageModule'
            }
          ]
      },
      {
        path: 'sensores',
        children:
          [
            {
              path: '',
              loadChildren: '../sensores/sensores.module#SensoresPageModule'
            }
          ]
      },
      {
        path: '',
        redirectTo: '/resevatorio/dados',
        pathMatch: 'full'
      }
    ]
},
{
  path: '',
  redirectTo: '/resevatorio/dados',
  pathMatch: 'full'
}
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ResevatorioPage]
})
export class ResevatorioPageModule {}
