import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResevatorioPage } from './resevatorio.page';

const routes: Routes = [
  {
    path: 'resevatorio',
    component:ResevatorioPage,
    children: [
      {
        path: 'dados',
        children: [
          {
            path: '',
            loadChildren: '../dados/dados.module#DadosPageModule'
          }
        ]
      },
      {
        path: 'especies',
        children: [
          {
            path: '',
            loadChildren: '../especies/especies.module#EspeciesPageModule'
          }
        ]
      },
      {
        path: 'sensores',
        children: [
          {
            path: '',
            loadChildren: '../sensores/sensores.module#SensoresPageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/resevatorio/dados',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/resevatorio/dados',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ResevatorioPageRoutingModule {}
