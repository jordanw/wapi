import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {AngularFireDatabase} from "angularfire2/database";
import 'rxjs/add/operator/map';
//import { map } from 'rxjs/operators';
import firebase from 'firebase';
import { Observable } from 'rxjs';
//import {Storage} from '@ionic/storage';

@Injectable()
export class RealdatabaseProvider {

  private PATHSPECIE = 'species/';
  //private PATHDATAIDEAL = 'data_ideal/';
  private PATHDATAIDEAL = 'data_collected/';
  constructor(
    public http: HttpClient,
    private db: AngularFireDatabase,
   // private storage: Storage
    ) {
    console.log('Hello RealdatabaseProvider Provider');
  }

  getAllSpecies()
  {
    return this.db.list(this.PATHSPECIE)
      .snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      })
  }

 
  getAllDataIdeal()
  {
    return this.db.list(this.PATHDATAIDEAL,  ref => ref.limitToLast(1))
      .snapshotChanges()
      .map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      })
  }

  getSpecie(key:string)
  {
    return this.db.object(this.PATHSPECIE + key).snapshotChanges()
      .map(c=>{
        return {key:c.key, ...c.payload.val()};
      })
  }

  getDataIdeal(key:string)
  {
    return this.db.object(this.PATHDATAIDEAL + key).snapshotChanges()
      .map(c=>{
        return {key:c.key, ...c.payload.val()};
      })
  }

  saveSpecie(specie:any)
  {
    return new Promise((resolve, reject) => {
      if (specie.uid) {
        this.db.list(this.PATHSPECIE)
          .update(specie.uid, {
            name_scientific: specie.name_popular,
            name_popular: specie.name_scientific,            
              })
          .then(() => resolve())
          .catch((e) => reject(e));
      } else {
        this.db.list(this.PATHSPECIE)
          .push({
            name_popular: specie.name_popular,
            name_scientific: specie.name_scientific,
            
          })
          .then(() => resolve());
      }
    })
  }

  saveDataIdeal(data:any)
  {
    return new Promise((resolve, reject) => {
      if (data.uid) {
        this.db.list(this.PATHDATAIDEAL)
          .update(data.uid, {
            //uid_specie: data.uid,
            time_stamp: data.time_stamp,
            temp:{
                max: data.temp.max,
                min: data.temp.min
            },
            cond: data.cond,
            ph: data.ph,
            od: data.od,
            sal: data.sal,
            ammonia: data.ammonia,
            turb: data.turb
          })
          .then(() => resolve())
          .catch((e) => reject(e));
      } else {
        this.db.list(this.PATHDATAIDEAL)
          .push({
            //uid_specie: data.uid,
            time_stamp: data.time_stamp,
            temp:{
                max: data.temp.max,
                min: data.temp.min
            },
            cond: data.cond,
            ph: data.ph,
            od: data.od,
            sal: data.sal,
            ammonia: data.ammonia,
            turb: data.turb            
          })
          .then(() => resolve());
      }
    })
  }

  removeSpecie(key:string)
  {
    return this.db.list(this.PATHSPECIE).remove(key);
  }

  removeDataIdeal(key:string)
  {
    return this.db.list(this.PATHDATAIDEAL).remove(key);
  }

}
