import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-devices',
  templateUrl: 'devices.html',
})
export class DevicesPage {

  device = {
    1:{
      name: 'Oxigênio Dissolvido',
      status: 'Inativo'
    },
    2:{
      name: 'Sensor de Temperatura Superfície',
      status: 'ativo'
    },

    3: {
      name: 'Sensor de Temperatura Coluna Dagua',
      status: 'ativo'
    },
    4: {
      name: 'Sensor de Temperatura Coluna Substrato',
      status: 'ativo'
    },

    5:{
      name: 'Sensor de PH',
      status: 'Ativo'
    },

    6:{
      name: 'Sensor de Condutividade',
      status: 'Ativo'
    },

    7:{
      name: 'Sensor de Salinidade',
      status: 'Inativo'
    },

    8:{
      name:'Sensor de Amônia',
      staus: 'Inativo'
    },
    9:{
      name: 'Sensor de Turbidez',
      status: 'Inativo'
    }
  };

  devices = ['Oxigênio Dissolvido', 'Sensor de Temperatura Sup.', 'Sensor de Temperatura Col.',
              'Sensor de Temperatura Sub.',
              'Sensor de PH', 'Sensor de Condutividade', 'Sensor de Salinidade',
              'Sensor de Amônia', 'Sensor de Turbidez'];

  status = ['Inativo', 'Ativo', 'Ativo',
              'Ativo', 'Ativo', 'Ativo',
              'Inativo', 'Ativo', 'Ativo'];

  constructor(public navCtrl: NavController, public navParams: NavParams)
  {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DevicesPage');
  }

}
