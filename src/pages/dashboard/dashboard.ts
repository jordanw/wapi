import {Component, ElementRef} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DataPage} from "../data/data";
import {RealdatabaseProvider} from '../../providers/realdatabase/realdatabase';
import {database} from '../../model/database.model';
import { Observable } from 'rxjs/Observable';

import { ToastController } from 'ionic-angular';
import { Network } from '@ionic-native/network';

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  public category;
  public categories;
  public wapi;
  color="green";
  showTilapia:boolean = false;
  showShrimp:boolean = false;
  toggleDisabled:string = "secondary";
  teste:Observable<any>
  //teste = {}
  /*
      ******* DATA FROM FIREBASE
   */
  slist: Observable<any>;
  public data_show:any = {}
  temp_sup
  temp_col
  temp_sub
  condutividade
  ph
  od
  salinidade
  ammonia:any

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private elRef:ElementRef,
    private provider:RealdatabaseProvider,
    private toast: ToastController,
    private network: Network
    )
  {
    //console.log('teste: ', this.teste)  
  }

  ionViewDidLoad() {
    this.wapi = "control";      
    console.log('ionViewDidLoad DashboardPage');     
     this.provider.getAllDataIdeal().subscribe((val)=>{
      //this.data_show = val
      this.ammonia = val[0]['ammonia']
      this.temp_sup = val[0]['temp']['sup']
      this.temp_sub = val[0]['temp']['sub']
      this.temp_col = val[0]['temp']['col']
      this.condutividade = val[0]['cond']
      this.ph = val[0]['ph']
      this.od = val[0]['od']
      this.salinidade = val[0]['sal']

    })
  }

  ngAfterViewInit() {

  }

  onTabChanged(tabName) {
    this.category = tabName;
  }

  showTilapiaM()
  {
    this.showTilapia = !this.showTilapia;
  }

  showShrimpMethod()
  {
    this.showShrimp = !this.showShrimp;
  }


  pushData()
  {
    return this.navCtrl.push(DataPage);
  }

  back()
  {
    return this.navCtrl.pop();
  }
  backPage()
  {
    return this.navCtrl.pop();
  }

  refresh(refresher) {
    this.ionViewDidLoad();
    refresher.complete();
  }

}

