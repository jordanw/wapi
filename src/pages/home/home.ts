import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {DashboardPage} from "../dashboard/dashboard";
//import {RealdatabaseProvider} from '../../providers/realdatabase/realdatabase';
import {database} from '../../model/database.model';
import { Observable } from 'rxjs/Observable';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  species = [
    "Curimata",
    "Pial",
    "Tilápia",
    "Tambaqui",
    "Tambacu",
    "Tambatinga",
    "Pintado",
    "Cachara",
    "Cacharipa",
    "Pintachara",
    "Surumbim",
    "Carpa comum",
    "Carpa chinesa",
    "Pirapitinga"

  ];
  


  slist: Observable<any>;
  data:any;
  data2:any;
  data_show = {}

  showSearch = false;
  testAdd = {
    ammonia:10,
    cond:10,
    od:10,
    ph:10,
    sal: 10,
    temp:{
      max:10,
      min:10
    },
    time_stamp: '2018-11-17 12:36:15',
    turb:10
  }

  constructor(
    public navCtrl: NavController,
    //private provider:RealdatabaseProvider
    ) 
  { 
    //this.provider.saveDataIdeal(this.testAdd)  
  }

  showSearchbar()
  {
    this.showSearch = !this.showSearch;
  }

  pushMonitoring()
  {
    this.navCtrl.push(DashboardPage);
  }

}
