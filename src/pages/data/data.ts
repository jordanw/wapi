import {Component, ElementRef, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Chart} from 'chart.js';

@IonicPage()
@Component({
  selector: 'page-data',
  templateUrl: 'data.html',
})
export class DataPage {
  @ViewChild('bshow') bshow:ElementRef;
  @ViewChild('graph') graph;
  show:boolean= false;
  g:any;

  constructor(public navCtrl: NavController, public navParams: NavParams)
  {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DataPage');
  }

  ngAfterViewInit()
  {
    console.log('testa elemento: ', this.bshow.nativeElement);
    console.log('graph elemento: ', this.graph.nativeElement);
   this.showGraph();
  }

  showGraph()
  {
    this.g = new Chart(this.graph.nativeElement, {
      type: 'line',
      scaleOverride : true,
      scaleStepWidth : 50,
      scaleStartValue : 0,
      data:{
        labels: ['Seg', 'Ter', 'Qua','Qui', 'Sex', 'Sab', 'Dom'],
        datasets:
          [{
            data: [10,20,30,7,12,15,3],
            label: "Superfícies",
            borderColor: "#FF7F50",
            backgroundColor: "#FF7F50",
          },
            {
              data: [29,25,22,27,30,35,33],
              label: "Coluna D'água",
              borderColor: "#FFA500",
              backgroundColor: "#FFA500",
            },

            {
              data: [10,12,14,17,12,15,13],
              label: "Substrato",
              borderColor: "#FFD700",
              backgroundColor: "#FFD700",
            }
          ],

        fill:false,
      },
      options:
        {
          maintainAspectRatio: false,
          title:{
            text:'Variação da temperatura',
            display: true
          },
          elements: {
            line: {
              fill: false,
            }
          },
          scales: {
            yAxes: [{
              ticks: {
                min: 0,
                max: 50
              }
            }]}

        }
    });
    this.g.canvas.parentNode.style.height = '250px';
  }

  showAside()
  {
    this.show = !this.show;
    if(this.show)
    {
      this.bshow.nativeElement.classList.remove('seta-direita');
      this.bshow.nativeElement.classList.add('seta-direita2');
    }else{
      this.bshow.nativeElement.classList.add('seta-direita');
      this.bshow.nativeElement.classList.remove('seta-direita2');
    }

  }




}
